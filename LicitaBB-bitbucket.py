# coding: utf-8

# IMPORTA BIBLIOTECAS E APIS


from selenium import webdriver
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.keys import Keys
import time
from twocaptchaapi import TwoCaptchaApi
from PIL import Image
from io import BytesIO
import os
import gspread
from oauth2client.service_account import ServiceAccountCredentials
from selenium.webdriver.chrome.options import Options
from datetime import date


#QTD ITEMS ATUALIZADOS
itemsAtualizados = 0

#DATA ATUAL
data_atual = date.today()
data_em_texto = data_atual.strftime('%d/%m/%Y')

#Dados para o selenium funcionar no windows
options = Options()
options.headless = True
options.add_argument('--no-sandbox')  # Bypass OS security model
options.add_argument('--disable-gpu')  # applicable to windows os only
options.add_argument('start-maximized')  #
options.add_argument('disable-infobars')
options.add_argument("--disable-extensions")

scope = ["https://spreadsheets.google.com/feeds", 'https://www.googleapis.com/auth/spreadsheets',
         "https://www.googleapis.com/auth/drive.file", "https://www.googleapis.com/auth/drive"]

creds = ServiceAccountCredentials.from_json_keyfile_name("credenciaislicitabb.json", scope)

client = gspread.authorize(creds)

sheet = client.open("Licitações.xlsx").sheet1

#INSERIR API DO TWOCAPTCHA

api = TwoCaptchaApi('INSIRA A API KEY AQUI')

# IMPORTA O ARQUIVO DE CONFIGURAÇÃO


ListaMercadorias = [line.rstrip('\n') for line in open("mercadorias.txt", encoding='utf-8-sig')]
print(ListaMercadorias)

# inicia o webdriver -- headless mode
driver = webdriver.Chrome(executable_path=r"chromedriver.exe", options=options)
#driver = webdriver.Chrome(options=options)

reiniciar = True



for mercadoria in ListaMercadorias:

    carregou = False

    while carregou == False:
        driver.get("http://www.licitacoes-e.com.br/aop/pesquisar-licitacao.aop?opcao=preencherPesquisar")
        try:
            time.sleep(5)
            driver.find_element_by_xpath('//*[@id="licitacaoPesquisaSituacaoForm"]/div[5]/span/a')
            carregou = True
            print("Carregou")
        except:
            carregou = False
            print("Erro de carregamento - tentando novamente...")
            pass
    print(mercadoria)
    # driver recebe a página de busca


    # In[191]:

    # Tempo para a página carregar
    time.sleep(10)

    # Seleciona o campo situação e o clica
    situacao = driver.find_element_by_xpath('//*[@id="licitacaoPesquisaSituacaoForm"]/div[5]/span/a')
    teste = situacao.click()

    # Insere valores inciais da situação da licitação, desce o cursor e aceita
    seletorSituacao = driver.find_element_by_xpath('//*[@id="licitacaoPesquisaSituacaoForm"]/div[5]/span/input')
    seletorSituacao.clear()
    seletorSituacao.send_keys("Acolhi")  # caracteres iniciais para 'acolhimento de propostas'
    seletorSituacao.send_keys(Keys.ARROW_DOWN)
    seletorSituacao.send_keys(Keys.ENTER)

    #Seleciona período a partir de arquivo de configuração
    seletorPeriodo = ""
    for line in open("config_periodo.txt", encoding="utf-8"):
        li = line.strip()
        if not li.startswith("#"):
            seletorPeriodo = (line.rstrip())
    seletorPeriodo

    if seletorPeriodo != "TUDO":
        # Seleciona o campo periodo e o clica
        situacao = driver.find_element_by_xpath('//*[@id="licitacaoPesquisaSituacaoForm"]/div[8]/span/input')
        teste = situacao.click()

        # Insere valores inciais do periodo, desce o cursor e aceita
        seletorSituacao = driver.find_element_by_xpath('//*[@id="licitacaoPesquisaSituacaoForm"]/div[8]/span/input')
        seletorSituacao.clear()

        if seletorPeriodo == "HOJE":
            seletorSituacao.send_keys("Hoj")  # caracteres iniciais
            seletorSituacao.send_keys(Keys.ARROW_DOWN)
            seletorSituacao.send_keys(Keys.ENTER)

        elif seletorPeriodo == "SEMANA_ATUAL":
            seletorSituacao.send_keys("Seman")  # caracteres iniciais
            seletorSituacao.send_keys(Keys.ARROW_DOWN)
            seletorSituacao.send_keys(Keys.ENTER)
        elif seletorPeriodo == "MES_ATUAL":
            seletorSituacao.send_keys("Mês")  # caracteres iniciais
            seletorSituacao.send_keys(Keys.ARROW_DOWN)
            seletorSituacao.send_keys(Keys.ENTER)
        else:
            pass

    # Insere o tipo de mercadoria a ser buscado
    campoMercadoria = driver.find_element_by_name("textoMercadoria")

    campoMercadoria.send_keys(mercadoria)

    # In[193]:

    # Captura imagem do captcha

    ImagemCaptcha = driver.find_element_by_id("img_captcha")

    ImagemCaptcha = ImagemCaptcha.screenshot_as_png

    # Abre a imagem
    imagem = Image.open(BytesIO(ImagemCaptcha))

    # salva imagem temporária do captcha
    imagem.save("captcha.png")

    # In[194]:

    # resolve captcha

    with open('captcha.png', 'rb') as captcha_file:
        captcha = api.solve(captcha_file)

    captchaTexto = captcha.await_result()

    # apaga imagem temporária
    os.remove("captcha.png")

    # In[195]:

    # insere a resposta do captcha
    campoCaptcha = driver.find_element_by_id("pQuestionAvancada")
    campoCaptcha.send_keys(captchaTexto)

    # Clica no botão de busca
    btnBusca = driver.find_element_by_name("pesquisar")
    btnBusca.click()

    # In[196]:

    # Tempo para a página carregar
    time.sleep(2)
    try:
        listaTudo = driver.find_element_by_name("tCompradores_length")
        Select(listaTudo).select_by_visible_text("Todos")
    except:
        pass


    # In[197]:

    # Retorna posição da primeira linha em branco

    def first_empty_row():
        all = sheet.get_all_values()
        row_num = 1
        consecutive = 0
        for row in all:
            flag = False
            for col in row:
                if col != "":
                    # something is there!
                    flag = True
                    break

            if flag:
                consecutive = 0
            else:
                # empty row
                consecutive += 1

            if consecutive == 2:
                # two consecutive empty rows
                return row_num - 1
            row_num += 1
        # every row filled
        return row_num


    # In[198]:

    # Lista de compradores

    Compradores = driver.find_elements_by_xpath('//td[@width="25%"]')
    listaCompradores = []
    x = 0
    while x < len(Compradores):
        listaCompradores.append(Compradores[x].text)
        x = x + 1

    # Lista de Num Licitação
    numLicitacao = driver.find_elements_by_xpath('//td[@width="10%"] [@valign="top"]')
    listaNumLicitacao = []
    x = 0
    while x < len(numLicitacao):
        listaNumLicitacao.append(numLicitacao[x].text)
        x = x + 1

    # Separa o detalhamento em listas de diversos elementos

    Detalhes = driver.find_elements_by_xpath('//td[@width="62%"] [@valign="top"]')

    listaObjeto = []
    listaUor = []
    listaUf = []
    listaModalidade = []
    listaNumEdital = []
    listaNumProcesso = []

    x = 0
    while x < len(Detalhes):
        textoSeparado = Detalhes[x].text.split('\n')

        Objeto = textoSeparado[0]
        Uor = textoSeparado[1].split('|')[0].split(':')[1].strip()
        Uf = textoSeparado[1].split('|')[1].split(':')[1].strip()
        Modalidade = textoSeparado[2].split(':')[1].strip()
        NumEdital = textoSeparado[3].split('|')[0].split(':')[1].strip()
        NumProcesso = textoSeparado[3].split('|')[1].split(':')[1].strip()

        listaObjeto.append(Objeto)
        listaUor.append(Uor)
        listaUf.append(Uf)
        listaModalidade.append(Modalidade)
        listaNumEdital.append(NumEdital)
        listaNumProcesso.append(NumProcesso)

        x = x + 1

    # In[199]:

    # Fim da planilha - primeira linha em branco
    linhaemBranco = first_empty_row()

    y = 0
    parada = 0

    # checa se o link já existe, para evitar dados duplicados
    listaLinksOriginais = sheet.col_values(11)

    print(mercadoria)
    print(listaNumLicitacao)

    # Itera as listas preenchendo os campos da planilha

    # Verifica se a lista não está vazia
    if listaNumLicitacao:
        while y < len(listaNumLicitacao):

            # print(y)
            # print(len(listaNumLicitacao))
            if not (
                           "http://www.licitacoes-e.com.br/aop/consultar-detalhes-licitacao.aop?opcao=exibirDetalhesLicitacao&numeroLicitacao=" +
                           listaNumLicitacao[y]) in listaLinksOriginais:

                # durma 20s a cada 5 linhas
                if parada == 5:
                    time.sleep(30)
                    parada = 0
                # print(mercadoria)
                print(listaObjeto[y])
                # Ordem: linha,coluna,texto

                # Campo B
                sheet.update_cell(linhaemBranco, 2, "BOT BB")

                # Modalidade + objeto
                sheet.update_cell(linhaemBranco, 3, listaModalidade[y] + " - " + listaObjeto[y])

                # Localidade
                sheet.update_cell(linhaemBranco, 4, listaUf[y])

                # Campo licitação
                sheet.update_cell(linhaemBranco, 5, listaModalidade[y] + " - " + listaNumEdital[y])

                # Campo UASG
                sheet.update_cell(linhaemBranco, 7, listaCompradores[y])

                # Campo Link
                sheet.update_cell(linhaemBranco, 11,
                                  "http://www.licitacoes-e.com.br/aop/consultar-detalhes-licitacao.aop?opcao=exibirDetalhesLicitacao&numeroLicitacao=" +
                                  listaNumLicitacao[y])

                linhaemBranco = linhaemBranco + 1

                itemsAtualizados = itemsAtualizados + 1

            y = y + 1
            parada = parada + 1

if itemsAtualizados > 0:

    # INICIO ENVIO DE EMAIL

    from email.mime.multipart import MIMEMultipart
    from email.mime.text import MIMEText
    import smtplib

    # create message object instance
    msg = MIMEMultipart()

    x = itemsAtualizados
    y = data_em_texto

    message = ("Painel de licitações atualizado por BOT-BB com " + str(x) + " itens em " + y)

    # setup the parameters of the message
    password = "SENHA DE EMAIL AQUI"
    msg['From'] = "contato@daudt.eng.br"
    msg['To'] = "contato@daudt.eng.br"
    msg['Subject'] = message

    # add in the message body
    msg.attach(MIMEText(message, 'plain'))

    # create server
    server = smtplib.SMTP('mail.daudt.eng.br: 587')

    server.starttls()

    # Login Credentials for sending the mail
    server.login(msg['From'], password)

    # send the message via the server.
    server.sendmail(msg['From'], msg['To'], msg.as_string())

    server.quit()

    print("successfully sent email to %s:" % (msg['To']))

    # FIM - ENVIO DE EMAIL

    # INICIO TWEET
    import tweepy


    #INSIRA DADOS DA API DO TWITTER AQUI
    # personal information
    consumer_key = ""
    consumer_secret = ""
    access_token = ""
    access_token_secret = ""

    # authentication
    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_token_secret)

    api = tweepy.API(auth)

    api.update_status(status=("Temos " + str(x) + " novas oportunidades de melhorar o país. Confira no nosso painel de licitações em online.daudt.eng.br/licitacoes"))
    # FIM TWEET



driver.quit()







